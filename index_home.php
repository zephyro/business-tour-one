<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">

        <link rel="stylesheet" href="js/vendor/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

            <div style="padding: 40px;">
                <a href="#" class="green_btn ticket-selection-toggle text-center">Open panel</a>
            </div>

        </div>

        <div class="ticket-selection-inner">

            <div class="ticket-selection">

                <div class="ticket-selection__header">
                    <span>Выбор билета</span>
                    <i class="ticket-selection__close ticket-selection-toggle"></i>
                </div>

                <div class="ticket-selection__content mCustomScrollbar" data-mcs-theme="minimal-dark">

                    <div class="ticket-selection__box">
                        <div class="ticket-selection__text">
                            <h4>У этого рейса парные билеты</h4>
                            <p>
                                Мы объединяем билеты от разных авиалиний чтобы предложить вам лучшие
                                варианты! Не забывайте, что все изменения и отмены должны быть
                                произведены <strong>для каждого билета в отдельности</strong>.
                            </p>
                        </div>
                    </div>

                    <div class="ticket-selection__box">

                        <div class="ticket-selection__route">
                            <div class="ticket-selection__route_start">
                                <strong>KZN</strong>
                                <div>пятница, <span>12 ноября</span></div>
                            </div>
                            <div class="ticket-selection__route_finish">
                                <strong>SVO</strong>
                                <div>воскресенье, <span>14 ноября</span></div>
                            </div>
                        </div>

                        <div class="ticket-selection__path">

                            <div class="ticket-selection__point ticket-selection__point_start">
                                <div class="ticket-selection__point_time">23:25</div>
                                <div class="ticket-selection__point_place"><strong>SVO</strong> <span>Москва, Шереметьево</span></div>
                            </div>

                            <div class="ticket-selection__info">
                                <div class="ticket-selection__info_line"></div>
                                <div class="ticket-selection__info_time">1ч 25мин</div>
                                <div class="ticket-selection__info_detail">
                                    <div class="ticket-selection__info_type">Эконом-класс</div>
                                    <div class="ticket-selection__info_flight">
                                        <span>SU 1165</span>
                                        <a href="#" class="button_info">?</a>
                                    </div>
                                </div>
                            </div>

                            <div class="ticket-selection__point ticket-selection__point_finish">
                                <div class="ticket-selection__point_time">11:28</div>
                                <div class="ticket-selection__point_place"><strong>SPB</strong> <span>Санкт-Петербург, Пулково</span></div>
                            </div>

                        </div>

                    </div>

                    <div class="ticket-selection__box">

                        <div class="ticket-selection__route">
                            <div class="ticket-selection__route_start">
                                <strong>KZN</strong>
                                <div>пятница, <span>12 ноября</span></div>
                            </div>
                            <div class="ticket-selection__route_finish">
                                <strong>SVO</strong>
                                <div>воскресенье, <span>14 ноября</span></div>
                            </div>
                        </div>

                        <div class="ticket-selection__path">

                            <div class="ticket-selection__point ticket-selection__point_start">
                                <div class="ticket-selection__point_time">23:25</div>
                                <div class="ticket-selection__point_place"><strong>SVO</strong> <span>Москва, Шереметьево</span></div>
                            </div>

                            <div class="ticket-selection__info">
                                <div class="ticket-selection__info_line"></div>
                                <div class="ticket-selection__info_time">1ч 25мин</div>
                                <div class="ticket-selection__info_detail">
                                    <div class="ticket-selection__info_type">Эконом-класс</div>
                                    <div class="ticket-selection__info_flight">
                                        <span>SU 1165</span>
                                        <a href="#" class="button_info">?</a>
                                    </div>
                                </div>
                            </div>

                            <div class="ticket-selection__point ticket-selection__point_change">
                                <div class="ticket-selection__point_time">01:15</div>
                                <div class="ticket-selection__point_place"><strong>TUL</strong> <span>Тула, ночная пересадка 2ч 30 мин</span></div>
                            </div>

                            <div class="ticket-selection__info">
                                <div class="ticket-selection__info_line"></div>
                                <div class="ticket-selection__info_time">1ч 25мин</div>
                                <div class="ticket-selection__info_detail">
                                    <div class="ticket-selection__info_type">Эконом-класс</div>
                                    <div class="ticket-selection__info_flight">
                                        <span>SU 1165</span>
                                        <a href="#" class="button_info">?</a>
                                    </div>
                                </div>
                            </div>

                            <div class="ticket-selection__point ticket-selection__point_finish">
                                <div class="ticket-selection__point_time">11:28</div>
                                <div class="ticket-selection__point_place"><strong>SPB</strong> <span>Санкт-Петербург, Пулково</span></div>
                            </div>

                        </div>

                    </div>

                    <div class="ticket-selection__box">

                        <div class="ticket-selection__route">
                            <div class="ticket-selection__route_start">
                                <strong>KZN</strong>
                                <div>пятница, <span>12 ноября</span></div>
                            </div>
                            <div class="ticket-selection__route_finish">
                                <strong>SVO</strong>
                                <div>воскресенье, <span>14 ноября</span></div>
                            </div>
                        </div>

                        <div class="ticket-selection__path">

                            <div class="ticket-selection__point ticket-selection__point_start">
                                <div class="ticket-selection__point_time">23:25</div>
                                <div class="ticket-selection__point_place"><strong>SVO</strong> <span>Москва, Шереметьево</span></div>
                            </div>

                            <div class="ticket-selection__info">
                                <div class="ticket-selection__info_line"></div>
                                <div class="ticket-selection__info_time">1ч 25мин</div>
                                <div class="ticket-selection__info_detail">
                                    <div class="ticket-selection__info_type">Эконом-класс</div>
                                    <div class="ticket-selection__info_flight">
                                        <span>SU 1165</span>
                                        <a href="#" class="button_info">?</a>
                                    </div>
                                </div>
                            </div>

                            <div class="ticket-selection__point ticket-selection__point_change">
                                <div class="ticket-selection__point_time">01:15</div>
                                <div class="ticket-selection__point_place"><strong>TUL</strong> <span>Тула, ночная пересадка 2ч 30 мин</span></div>
                            </div>

                            <div class="ticket-selection__info">
                                <div class="ticket-selection__info_line"></div>
                                <div class="ticket-selection__info_time">1ч 25мин</div>
                                <div class="ticket-selection__info_detail">
                                    <div class="ticket-selection__info_type">Эконом-класс</div>
                                    <div class="ticket-selection__info_flight">
                                        <span>SU 1165</span>
                                        <a href="#" class="button_info">?</a>
                                    </div>
                                </div>
                            </div>

                            <div class="ticket-selection__point ticket-selection__point_finish">
                                <div class="ticket-selection__point_time">11:28</div>
                                <div class="ticket-selection__point_place"><strong>SPB</strong> <span>Санкт-Петербург, Пулково</span></div>
                            </div>

                        </div>
                        <div class="mb_20"></div>
                        <div class="ticket-selection__text">
                            <h4>Невозвратный  билет</h4>
                            <p>Его невозможно поменять, если вы опоздаете на рейс или передумаете лететь.</p>
                        </div>

                    </div>

                    <div class="ticket-selection__box">
                        <div class="ticket-selection__title">Багаж</div>

                        <div class="ticket-selection__baggage">
                            <div class="ticket-selection__baggage_logo ticket-selection__baggage_logo_rgd"></div>
                            <div class="ticket-selection__baggage_route">
                                <span>Казань - Москва</span>
                            </div>
                            <div class="ticket-selection__baggage_info">
                                <div class="ticket-selection__baggage_item baggage_hand_plus">Ручная кладь, 40 х 60 х 120, до 5 кг</div>
                                <div class="ticket-selection__baggage_item baggage_minus">Багаж не включен в стоимость<span></span>
                                </div>
                            </div>
                        </div>

                        <div class="ticket-selection__baggage">
                            <div class="ticket-selection__baggage_logo ticket-selection__baggage_logo_rgd"></div>
                            <div class="ticket-selection__baggage_route">
                                <span>Москва - Санкт-Петербург</span>
                                <strong>3 места</strong>
                            </div>
                            <div class="ticket-selection__baggage_info">
                                <div class="ticket-selection__baggage_item baggage_hand_plus">Ручная кладь, 40 х 60 х 120, до 5 кг </div>
                                <div class="ticket-selection__baggage_item baggage_plus">Багаж включен в стоимость</div>
                            </div>
                        </div>

                        <div class="ticket-selection__baggage">
                            <div class="ticket-selection__baggage_logo ticket-selection__baggage_logo_rgd"></div>
                            <div class="ticket-selection__baggage_route">
                                <span>Казань - Москва</span>
                            </div>
                            <div class="ticket-selection__baggage_info">
                                <div class="ticket-selection__baggage_item baggage_hand_plus">Ручная кладь, 40 х 60 х 120, до 5 кг</div>
                                <div class="ticket-selection__baggage_item baggage_minus">Багаж не включен в стоимость</div>
                            </div>
                        </div>

                        <div class="ticket-selection__baggage">
                            <div class="ticket-selection__baggage_logo ticket-selection__baggage_logo_rgd"></div>
                            <div class="ticket-selection__baggage_route">
                                <span>Москва - Санкт-Петербург</span>
                            </div>
                            <div class="ticket-selection__baggage_info">
                                <div class="ticket-selection__baggage_item baggage_hand_plus">Ручная кладь, 40 х 60 х 120, до 5 кг</div>
                                <div class="ticket-selection__baggage_item baggage_minus">Багаж не включен в стоимость</div>
                            </div>
                        </div>

                    </div>

                    <div class="ticket-selection__box">
                        <div class="ticket-selection__checkout">
                            <div class="ticket-selection__checkout_left">
                                <div class="ticket-selection__alert">
                                    <span>Вне тревел-политики</span>
                                    <a href="#" class="button_info button_info_alert">!</a>
                                </div>
                                <div class="ticket-selection__price">
                                    <strong>2500₽</strong>
                                    <div>итоговая цена</div>
                                </div>
                            </div>
                            <div class="ticket-selection__checkout_right">
                                <button type="submit" class="green_btn">Добавить в поездку</button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="ticket-selection-layout ticket-selection-toggle"></div>

        </div>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <script src="js/vendor/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
